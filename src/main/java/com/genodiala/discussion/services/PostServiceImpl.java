package com.genodiala.discussion.services;


import com.genodiala.discussion.models.Post;
import com.genodiala.discussion.repositories.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

@Service
// The @Service annotation will allow us to use the CRUD methods inherited from the CRUDRepository even though interface do not contain implementation/method bodies
public class PostServiceImpl implements PostService {

    // @Autowired indicates that Spring should automatically inject an instance of the PostRepository into the postRepository
    @Autowired
    // postRepository is a variable that represents the instance of PostRepository
    private PostRepository postRepository;

    //Create post
    public void createPost(Post post){
        postRepository.save(post);
    }
    //getPost
    public Iterable<Post> getPosts(){
        return postRepository.findAll();
    }

    //DeletePost
    public ResponseEntity deletePost(Long id){
        postRepository.deleteById(id);
        return new ResponseEntity<>("Post deleted successfully", HttpStatus.OK);
    }

    //Update post

    public ResponseEntity updatePost(Long id, Post post) {
        Post postForUpdating = postRepository.findById(id).get();
        postForUpdating.setTitle(post.getTitle());
        postForUpdating.setContent(post.getContent());

        postRepository.save(postForUpdating);

        return new ResponseEntity<>("Post updated successfully", HttpStatus.OK);
    }
}
