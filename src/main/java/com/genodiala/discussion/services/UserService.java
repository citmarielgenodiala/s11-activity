package com.genodiala.discussion.services;

import com.genodiala.discussion.models.User;
import org.springframework.http.ResponseEntity;

public interface UserService {

    void createUser(User user);

    Iterable<User> getUsers();

    ResponseEntity deleteUser(Long id);

    ResponseEntity updateUser(Long id, User user);
}
