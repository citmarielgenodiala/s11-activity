package com.genodiala.discussion.services;

import com.genodiala.discussion.models.Post;
import org.springframework.http.ResponseEntity;

public interface PostService {

    void createPost(Post post);

    //We can iterate/leap over the posts in
    Iterable<Post> getPosts();

    ResponseEntity deletePost(Long id);

    ResponseEntity updatePost(Long id, Post post);
}
