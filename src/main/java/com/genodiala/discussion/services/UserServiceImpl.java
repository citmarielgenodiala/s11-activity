package com.genodiala.discussion.services;


import com.genodiala.discussion.models.User;
import com.genodiala.discussion.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {


    @Autowired
    private UserRepository urepo;

    //Create user
    public void createUser(User user){
        urepo.save(user);
    }
    //Read users
    public Iterable<User> getUsers(){
        return urepo.findAll();
    }

    //Delete user
    public ResponseEntity deleteUser(Long id){
        urepo.deleteById(id);
        return new ResponseEntity<>("User with userID " + id+ " deleted successfully", HttpStatus.OK);
    }

    //Update user

    public ResponseEntity updateUser(Long id, User user) {
        User updateUserInfo = urepo.findById(id).get();
        updateUserInfo.setUsername(user.getUsername());
        updateUserInfo.setPassword(user.getPassword());
        urepo.save(updateUserInfo);
        return new ResponseEntity<>("User with userID " + id+ " updated successfully", HttpStatus.OK);
    }
}
