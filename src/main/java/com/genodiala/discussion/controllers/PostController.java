package com.genodiala.discussion.controllers;

import com.genodiala.discussion.models.Post;
import com.genodiala.discussion.services.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

//@RestController incoming HTTP request and map them to the corresponding methods in the controller
@RestController
//@CrossOrgin - CORS Cross Origin Resource Sharing
//Allows request from a different domain to be made to the application
@CrossOrigin
@RequestMapping("/posts")
public class PostController {

    @Autowired
    PostService postService;

    //Creating a post
//    @RequestMapping(value = "/posts", method = RequestMethod.POST);
    @PostMapping("/addPosts")
    public ResponseEntity<Object> createPost(@RequestBody Post post){
        postService.createPost(post);
        return new ResponseEntity<>("Post created successfully", HttpStatus.CREATED);
    }

   @GetMapping("/getPosts")
    public ResponseEntity<Object> getPosts(){
        return new ResponseEntity(postService.getPosts(), HttpStatus.OK);
   }

   @DeleteMapping("/deletePost/{postId}")
    public ResponseEntity<Object> deletePost(@PathVariable Long postId){
        return postService.deletePost(postId);
   }

    @PutMapping("/updatePost/{postId}")

    public ResponseEntity<Object> updatePost(@PathVariable Long postId, @RequestBody Post post){
        return postService.updatePost(postId, post);
    }

}
