package com.genodiala.discussion.controllers;


import com.genodiala.discussion.models.User;
import com.genodiala.discussion.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@CrossOrigin
//@RequestMapping("/users")
public class UserController {

    @Autowired
    UserService userv;

    @PostMapping("/users")
    public ResponseEntity<Object> createUser(@RequestBody User user){
        userv.createUser(user);
        return new ResponseEntity<>("User created successfuly", HttpStatus.CREATED);
    }

    @GetMapping("/users")
    public ResponseEntity<Object> getUsers(){
        return new ResponseEntity(userv.getUsers(), HttpStatus.OK);
    }

    @DeleteMapping("/users/{userId}")
    public ResponseEntity<Object> deleteUser(@PathVariable Long userId){
        return userv.deleteUser(userId);
    }

    @PutMapping("/users/{userId}")
    public ResponseEntity<Object> updateUser(@PathVariable Long userId, @RequestBody User user){
        return userv.updateUser(userId, user);
    }

}
